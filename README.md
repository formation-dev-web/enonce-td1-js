
TD1 - Javascript
================

Exo 1 - Hello World
-------------------

- a.  Modifiez exo-1.html pour le faire inlcure un script javascript qui affiche
  une popup avec "Bonjour le Monde" à l'utilisateur (utiliser `alert()`).

- b. Modifiez le script pour insérer "Bonjour le monde" dans le `<p>` plutôt que
  de l'afficher dans une popup.

Exo 2 - Cube
------------

On va utiliser JavaScript pour permettre à l'utilisateur de calculer le volume
d'un cube.

Le principe est simple : l'utilisateur entre le côté d'un cube dans un champ de
formulaire puis clique sur *Calculer*, on lui affiche la valeur dans le document.

*NB Le volume d'un cube vaut sont côté élevé à la puissance 3.*

Des fichiers *exo2.js* et *exo2.html* de base sont fournis, le code de gestion
d'évènement (réagir au clic) est déjà fourni.

### a. Action !

Remplir le code des fonctions `calculVolume()` et `miseAJourVolume()` dans *exo2.js* (inutile de modifier le code HTML
pour cette première partie):

- `calculVolume()` ne touche pas au *DOM*, elle se contente de faire un calcul mathématique (retourne le volume pour un côté donné)
- `miseAJourVolume()` appelle récupère le contenu du champ de formulaire, fait
appel `calculVolume` et injecte le résultat au bon endroit du DOM


### b. Tableau de résultats

Nous allons utiliser à la place d'un `<p>` un tableau pour présenter nos
résultats.

- Modifier le code HTML pour insérer un tableau (balise `<table>`, qui
  ne contient pour l'instant que des cases vides à la place des valeurs
  numériques.
- Modifier le JavaScript pour venir *remplir* les bonnes cases du tableau
  une fois le calcul effectué.

Le tableau aura ce genre de structure (un peu de CSS peut être nécessaire) :

![Exemple de présentation en tableau](ex-tableau.png)

- Une fois que le tableau est rempli, on peut en profiter pour effacer le
  contenu du champ de formulaire.


Exo 3 - Visualisateur de couleurs.
----------------------------------

Réaliser un sélecteur de couleurs :

- Un formulaire avec :
  - 3 champs (rouge vert bleu) de type "nombre"
  - un bouton de validation
- un rectangle coloré (via sa propriété `background-color`).


- Faire en sorte qu'au clic sur le bouton, le rectangle prenne la couleur RGB
  correspondant aux valeurs du Rouge, vert et bleu.
- Changer le code pour mettre à jour la couleur dès qu'une valeur est saisie
  (plus besoin de cliquer sur le bouton).
- Utiliser les attributs HTML5 appropriés pour empêcher la saisie de valeurs
  supérieures à 255 ou inférieures à zéro.


Exo 4 Catch me ! BONUS
----------------------

En prenant pour base le document *exo4.html*


- faire en sorte, en javascript,  que
le chat se déplace dès que le pointeur de la souris la survole, afin de ne plus
être sous le pointeur ; le chat ne doit jamais sortir de son conteneur (bordure
jaune).
- Jouer un son à chaque fois que le chat se déplace
- Introduire un délai de 100ms avant que le chat ne se déplace, laissant à
  l'utilisateur un court temps pour cliquer. Afficher un message de succès si
  l'utilisateur a le temps de cliquer sur le chat.


### Aides :

- On joue sur les attributs CSS `left` et `top`.
- On va avoir besoin de transformer la valeur des propriétés *top* et *right*
  en chiffres et vice-versa, regarder du côté de la fonction `parseInt` et la
  méthode `toString` sur les chaînes de caractère.
